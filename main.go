package main

import (
	"fmt"
	"github.com/bwmarrin/discordgo"
	"github.com/jonas747/dca"
	"github.com/kkdai/youtube/v2"
	"io"
	"os"
	"os/signal"
	"regexp"
	"strings"
	"syscall"
	"time"
)

var buffer = make([][]byte, 9)

// Remember to add comments.
func main() {
	dim, err := discordgo.New("Bot " + "token here")
	if err != nil {
		fmt.Println("Can't create bot session: ", err)
	}
	err = dim.Open()
	if err != nil {
		fmt.Println("Can't open Discord session: ", err)
	}

	dim.AddHandler(messageCheck)

	defer func() {
		sigChan := make(chan os.Signal, 1)
		signal.Notify(sigChan, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
		<-sigChan
	}()
}

func messageCheck(s *discordgo.Session, m *discordgo.MessageCreate) {
	if m.Author.ID == s.State.User.ID {
		return
	}

	if strings.HasPrefix(m.Content, "-dim") {
		c, err := s.State.Channel(m.ChannelID)
		if err != nil {
			return
		}

		g, err := s.State.Guild(c.GuildID)
		if err != nil {
			return
		}

		url := strings.TrimPrefix(m.Content, "-dim ")

		for _, vs := range g.VoiceStates {
			if vs.UserID == m.Author.ID {
				fmt.Println("trying to play: ", url)
				err = playSound(s, g.ID, vs.ChannelID, url)
				if err != nil {
					fmt.Println("Error playing sound: ", err)
				}

				return
			}
		}
	}
}

func playSound(s *discordgo.Session, guildID, channelID string, url string) (err error) {
	vc, err := s.ChannelVoiceJoin(guildID, channelID, false, true)
	if err != nil {
		return err
	}

	time.Sleep(250 * time.Millisecond)

	opts := dca.StdEncodeOptions
	opts.RawOutput = true
	opts.Bitrate = 64
	opts.Application = "lowdelay"

	client := youtube.Client{}

	re := regexp.MustCompile(`((?:https?:)?\/\/)?((?:www|m)\.)?((?:youtube\.com|youtu.be))(\/(?:[\w\-]+\?v=|embed\/|v\/)?)([\w\-]+)(\S+)`)
	yt := re.ReplaceAllString(url, "$5$6")

	fmt.Println(yt)
	video, err := client.GetVideo(yt)
	format := video.Formats.FindByItag(250)
	if err != nil {
		panic(err)
	}

	streamURL, err := client.GetStreamURL(video, format)
	if err != nil {
		panic(err)
	}

	encodeSession, err := dca.EncodeFile(streamURL, opts)
	fmt.Println(streamURL)
	if err != nil {
		fmt.Println("Error creating encode session: ", err)
	}
	defer encodeSession.Cleanup()

	err = vc.Speaking(true)
	if err != nil {
		fmt.Println("Error setting speaking: ", err)
	}

	defer vc.Speaking(false)

	done := make(chan error)
	_ = dca.NewStream(encodeSession, vc, done)
	err = <-done
	if err != nil && err != io.EOF{
		panic(err)
	}

	return err
}
